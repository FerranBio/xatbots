


switch() {
  
  ....

  case '/meteo': 
   var enviat = meteo(id_usuari,idioma); 
   break; 

}






function meteo(id,idioma)
{
  
var id=   id || id_grup ;  //Per adaptar si volem enviar al grup amb un activador d'horari 
var idioma =  idioma || "ca" ; // Per assignar idioma en cas de fer servir activador 

 var coor_lat = "41.398234";  // la latitud del nostre institut // Dep. Ensenyament Via Augusta 

 var coor_long = "2.139691";  // La longitud del nostre Institut


  var API_KEY = "" ; // API Key que obtindrem al registrarnos a https://home.openweathermap.org/users/sign_up     
  var url_meteo = "http://api.openweathermap.org/data/2.5/weather?lat=" + coor_lat + "&lon=" + coor_long + "&APPID=" +API_KEY + "&units=metric&lang="+idioma;
 

  var response_meteo = UrlFetchApp.fetch(url_meteo);   // Anirem a la web de meteo i recuperarem la informació a la variable response_meteo
  var json = response_meteo.getContentText();       //  Convertim el resultat obtingut que serà un fitxer en format JSON a una variable de text
  var dades = JSON.parse(json);       //Parsejem, que consistirà en convertir el resultat a files i columnes
 
  Logger.log(dades); //Fem un log per veure el resultat si es produís alguna errada

  // Per recuperar les dades haurem de fer servir els noms i les rutes que podem veure en el quadre anterior  

 // Llegim les dades que ens interessen

  var lloc = dades.name;
  var temperatura = dades.main.temp;
  var pressio = dades.main.pressure;
  var humitat = dades.main.humidity;
 
 
  var icona = dades.weather[0].icon;  // obtenim el nom de la icona representativa del temps
  var general = dades.weather[0].description;
 
 // Construim la sortida de dades al nostre canal

  var resposta_meteo = "[" + lloc + "]  %0A" ; // els salts de linia en un caption es fan amb "%0A"
  resposta_meteo = resposta_meteo + "Temperatura actual = " + temperatura + " ºC %0A" ;
  resposta_meteo = resposta_meteo + "Humitat = " + humitat + "%25" + "  %0A" ;
  resposta_meteo = resposta_meteo + "Pressió =  " + pressio + " mb  %0A" ;
  resposta_meteo = resposta_meteo + "Temps general = " + general + " %0A" ;  
     
  var icona_meteo = "http://openweathermap.org/img/wn/" + icona + "@2x.png" ;  // Construim la ruta del fitxer amb la imatge del temps
 
 
sendPhoto(id,icona_meteo, resposta_meteo);

}

