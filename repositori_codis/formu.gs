/*

Nombre del script = formu.gs
Autor = @FerranMas  / profefp.tk@gmail.com 
Fecha primera versió = 14/07/2022

Este script utiliza el API de https://latex.codecogs.com  que enviando como parámetro la formula LaTeX nos devuelve la imagen de dicha formula. 
Por la dificultad de conseguir el relleno de la imagen con fondo blanco se ha optado por obtener la salida en formato SVG,  pero ello implica alguna modificación añadida porque el formato SVG no lo muestra Telegram y lo deberemos pasar a PNG 

Para usar este comando en Telegram usaremos la siguiente sintaxi : 
/formu        - El comando de Telegram seguido de un espacio
##            - Separador entre comando y formula 
codigo LaTex  - Código LaTeX de la formula que queremos representar 

Ejemplo : /formu ##\int_{a}^{b} x dx 


Pasamos como parámetros : 
id = Id de Telegram  del xat donde mostrar la formula 
text = El texto que se ha escrito 

*/



function formula(id,text)
{
var folderId = ""; // Id de la carpeta del Drive donde grabará los archivos temporales 

var formu = text.split("##")[1]; // Separamos el trozo de comando de Telegram de la formula LaTeX

// Vamos a usar la API del webservice https://latex.codecogs.com 
// Usaremos el formato de salida SVG porque los otros formatos gráficos usan un fondo de relleno negro 
// Si pudiéramos configurar el relleno blanco       simplificaría el proceso 

// Construimos la url de acceso a la API como en la formula pude contener caracteres especiales deberemos codificar la URL 
var url0 = "https://latex.codecogs.com/svg.image?%5Cbg%7Bwhite%7D"+ encodeURIComponent(formu)   ; 

var img = UrlFetchApp.fetch(url0).getBlob() ; // Accedemos a la url y recuperamos los datos en binario con BLOB 

// Como el formato recibido es SVG i no se muestra en Telegram, haremos lo siguiente : 
// Lo gravaremos en la carpeta temporal de Drive 
// Luego como Drive genera una imagen PNG del contenido de cada archivo, usaremos esta imagen 


var file = DriveApp.getFolderById(folderId).createFile(img);  // Creamos el fichero SVG recibido como BLOB 
var fileId = file.getId();  //  Recopilamos el id del fichero recién creado para obtener su url 
  
Utilities.sleep(10000);   // Añadimos una espera de lo contrario no da tiempo y da error 
// var url = Drive.Files.get(fileId).thumbnailLink.replace("=s200", "=s100") ;  // Podemos modificar el tamaño 
var url = Drive.Files.get(fileId).thumbnailLink ; // Obtenemos la url de la imagen PNG que muestra el contenido del fichero gravado 
var blob2= UrlFetchApp.fetch(url).getBlob(); // Recuperamos la imagen ahora en formato PNG en su forma binaria con BLOB 
  
 
sendPhoto3(id,blob2,formu); // Enviamos a Telegram la imagen en formato binario y como caption la formula usada 
 Drive.Files.remove(fileId); // Eliminamos el archivo temporal 
 
 }



// Por si no tienen una función de enviar imagen a partir de una variable BLOB aquí tienen una muestra 

function sendPhoto3(chatId,blob2,caption){

  var payload = {
          method: "sendPhoto",
          chat_id: String(chatId),
          photo: blob2,
          caption : caption,
          parse_mode: "HTML"
    
           };
 
  var options = {
    method: "POST",
    payload: payload,
    muteHttpExceptions : true
  };
     
   var request = UrlFetchApp.fetch( telegramUrl + '/', options);
   }

