/* 
Nom de l'script = questionari.gs
Autor = @FerranMas  / profefp.tk@gmail.com 
Data primera versió = 15/09/2021

Aquest codi permet gestionar un questionari de preguntes emprant l'eina de questionaris de Telegram (sendPoll) : 
1.- Les preguntes les tindrem en una fulla del full de càlcul en el codi teniu com a !Quizzes) i en un altre full tindrem la llista d'alumnes amb el seu id de Telegram 
2.- Haurem d'afegir una funció per enviar questionaris a Telegram, serà la funció sendPoll. 
3.- Dins de la funció doPost haurem de controlar les respostes que faci el usuari, ho farem llegint el paràmetre "data.poll_answer"   :  if(data.poll_answer && data.poll_answer.user.id)  
4.- Per seguir les puntuacions farem servir la memoria cache de GAS , que ens permet guardar informació d'una sessió. La comanda "PropertiesService.getScriptProperties().getProperty(id_usuari) ;" l'etiquetem de manera pesonalitzada amb el id del usuari, amb aquesta guardarem les preguntes que hem fet i com que hem de guardar les respostes correctes crearem una segona variable en memoria, en aquest cas farem servir "PropertiesService.getScriptProperties().getProperty(id_usuari+"_2") ;"
5.- Utilitzarem una comanda "/ini_quiz" per esborrar les dades de les dues variables de memoria, fent servir la comanda "PropertiesService.getScriptProperties().deleteProperty(id_usuari);"  
6.- Per iniciar el questionari farem servir la comanda "/quiz" 





*/


/*
 Afegim la funció "SendPoll" que ens permetra gestionar les peguntes com si fossin enquestes de Telegram , podriem aprofitar aquesta mateixa funció per crear enquestes personalitzades 
*/

function sendPoll(chatId,text,options,type,correct)
{

/* Els paràmetres que rep la funció son : 
chatId : Id del xat a on s'ha de mostrat 
text : Enunciat a mostrar 
options : Array amb les possibles respostes 
type : Tipus d'enquesta 
correct : quina posició ocupa la fresposta correcta 
*/


       var data = {
      method: "post",
      payload: {
         method: "sendPoll",
         chat_id: String(chatId),
         question: text,
         type: type, 
         correct_option_id: correct ,
         open_period : time_quiz , 
         is_anonymous: false , 
         options: JSON.stringify(options)
       }
    }
    UrlFetchApp.fetch( telegramUrl + '/', data);

 }










function doPost(e) 
{
...

try{  

 if(data.poll_answer && data.poll_answer.user.id) 

  {
   var id_usuari = data.poll_answer.user.id ; 
   var usuari = data.poll_answer.user.first_name ; 
   
  var resposta = data.poll_answer.option_ids[0]; 
   
  var preguntes = PropertiesService.getScriptProperties().getProperty(id_usuari) ;
  var encerts = PropertiesService.getScriptProperties().getProperty(id_usuari+"_2"); 
  if(encerts == null) var encerts = 0; 
  
  var res = preguntes.split(','); 
  var ultima = res[res.length - 2]; 
  
  var final = ultima.split('//'); 
  var bona = final[1]; 
  
   var llista = new Array([{"text" : "Next Quiz", "callback_data": "/quiz" } ]) ;  
   var tecles =  { inline_keyboard: llista    , resize_keyboard: true,one_time_keyboard : true  }; 
  
  if(resposta == bona )  
         {
        
         encerts++;
         sendText(id_usuari,usuari + ", the answer is <b>Correct</b> . Do you have : " + parseInt(encerts) + " Success",tecles); 
          PropertiesService.getScriptProperties().setProperty(id_usuari+"_2" , encerts); 
         }
         else 
         {
           
          sendText(id_usuari,usuari + ", the answer <b>isn't correct</b> . Do you have : " + parseInt(encerts) + " Success",tecles); 
         }
  return ;   
  }
  }
  catch(err){
    Logger.log(err); 
  }  
   

 ...


}


   

.... 

// Caldrà afegir la comanda "/inscri"  dins del switch 

switch(comanda) 
{
  
...

 case '/quiz' : 
             var enviat = quiz(id_usuari,usuari); 
             break; 

 case '/ini_quiz': 
            PropertiesService.getScriptProperties().deleteProperty(id_usuari); 
            PropertiesService.getScriptProperties().deleteProperty(id_usuari+"_2");
            var enviat = quiz(id_usuari,usuari); 
            break; 

... 
}


/* 
En primer lloc la funció inscri ens recull la petició d'inscripció i cal que la comanda /inscri estigui acompanyada d'ún email per fer el registre : sigui "/inscri email"   
*/
