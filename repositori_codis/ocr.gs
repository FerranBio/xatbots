/* 
Nom de l'script = ocr.gs
Autor = @FerranMas  / profefp.tk@gmail.com 
Data primera versió = 24/10/2021

Aquest codi permet recollir una imatge enviada a Telegram i convertir a Text tots els caràcters . 
Fa servir el conversor de Google Docs , de manera que agafa la imatge en binari i la grava en un document de Google Docs emprant el parametre "ocr" en la gravacio. 
Una vegada creat el document recupera el text com a tal. 
Important que cal enviar la imatge com a imatge i no com a fitxer. 

*/






function doPost(e) 
{
...




try{

 if(data.message.photo)  // En cas de que enviem una imatge
{
var text = data.message.caption;  // Recupera el text de la imatge
var id = data.message.chat.id;  // Recupera el id de la finestra d'on procedeix el missatge
var id_usuari = data.message.from.id; // Recupera el id de l'usuari que ha escrit el missatge
var id_missatge = data.message.message_id; // Recupera el id del missatge
var update_id =   data.update_id; // Recupera el id del missatge final
var lang = data.message.from.language_code ;  // Recupera l'idioma que te el Telegram de l'usuari que ha enviat el missatge
var nom = data.message.from.first_name ;  // Recupera tot el nom de l'usuari que ha enviat el missatge
var location = data.message.location;
 
 
 
var file_id= data.message.photo[3].file_id;   // Important que indiquem la fila 3 perque es un array una llista i el quart element te la millor ressolució

var url_api_document = telegramUrl + "/getFile?file_id=" + file_id ;  // Demanem a Telegram més informació sobre aquesta imatge, per conèixer la seva url per fer-ne la descarrega

  var response_document = UrlFetchApp.fetch(url_api_document);  // Accedeix a la informació de Telegram sobre la imatge

  var json_document= response_document.getContentText(); // Agafa informació rebuda coma resposta de text

  var dades2 = JSON.parse(json_document); //Parseja el text rebut
 

  var path_document = dades2.result.file_path;  // Construim la url del document final
 

 var documentUrl = "https://api.telegram.org/file/bot" + token + "/" + path_document ; // Accedim a aquest fitxer enviat
 
 var image = UrlFetchApp.fetch(documentUrl).getBlob();  // Carreguem la imatge a una variable binaria 
 
  var file = {
    title: 'OCR File',
    mimeType: 'image/png'
  };  // configurem el nom i el tipus del document 

  // Gravem la variable image en un Google Docs amb el servei OCR activat 
  file = Drive.Files.insert(file, image, {ocr: true});

    var doc = DocumentApp.openById(file.id)  ;  // Asignem el fiter que hem creat amb el text 
   var texto = doc.getBody().getText() ; // Llegim el text del Document creat 

    DriveApp.getFileById(file.id).setTrashed(true); // Eliminem document creat com a intermediari 
   
   sendText(id,texto);  // Mostrem a Telegram el text obtingut 
 
    return ;    // Finalitza el doPost perque no cal que segueixi 
  
 }
  }
  catch(err){
    Logger.log(err); 
  }  
  

 // Fi recollida imatges 


