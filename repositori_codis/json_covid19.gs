/* 
Nom de l'script = json_covid19.gs
Autor = @FerranMas  / profefp.tk@gmail.com 
Data primera versió = 94/11/2020

Aquest codi permet accedir a nles dades d'un dataset en format JSON 

En concret a dos datasets : 
  A).- Dades de Covid19 dels centres educatius : 
  https://analisi.transparenciacatalunya.cat/resource/fk8v-uqfv.json?codcentre=

El dataset de dades Covid no retorna el nom ni la població del centre, per això fem una consulta a un segon dataset : 

  B).- Dades dels centres escolars a partir del codi de centre 
   https://analisi.transparenciacatalunya.cat/resource/3u9c-b74b.json?codi_centre=

En aquest exemple podeu veure com interactuar amb un webservice de dades obertes que retorna la consulta en format JSON 

*/


function covid_escola(id,codi_centre)
{

// 1r a partir del codi de centre fem una consulta via una primera API per tenir el nom i la població 

 var url = "https://analisi.transparenciacatalunya.cat/resource/3u9c-b74b.json?codi_centre="+codi_centre; //Cerca dades del centre 
     
     var request = UrlFetchApp.fetch( url);  // Enviem les dades a l'API i recuperem la resposta 
     var data = JSON.parse(request) ;  // Com la resposta es en format JSON la parsegem per obtenir files i columnes 

      if(data.length == 0 ) {  // Si no ens retorna dades 
          this.sendText(id,"Codi de centre erroni [" + codi_centre + "]"); 
          return false; 
         }
         else { // Si retorna dades agafem el nom del centre i la població 
         var nom_centre = data[0].denominaci_completa ; 
         var poblacio = data[0].nom_municipi; 
         }
         
// 2n A partir del codi de centre en una segona API per obtenir les dades covid19 del centre 

var url = "https://analisi.transparenciacatalunya.cat/resource/fk8v-uqfv.json?codcentre="+codi_centre; 
        
     var request = UrlFetchApp.fetch( url);  // Enviem les dades a l'API i recuperem la resposta 
     var data = JSON.parse(request) ;  // Com la resposta es en format JSON la parsegem per obtenir files i columnes 
   
     /* Com pot haver-hi un decalatge de dies en actualitzar el dataset, no fem la consulta per data sino per posició. El priemr que ens mostrarà serà el darrer que haurà entrat de manera que agafarem la posició 0 de la matriu(array) 
     */

     var dades = data[0];  // Agafem la fila 0 que serà la ultima actualització 
     var p = [Object.keys(dades)]; // agafem la capçalera de les etiquetes de l'objecte que ens han facilitat 
     var params =  p[0]; //A la llista 0 de la capçalera tenim els noms dels parametres 
     
     var frase = nom_centre + " [" + poblacio + "] \n";  // Construiem la frase de retorn 
     
// Creem un bucle per recorrer tota al llista de resultat 
     for(i=0; i < params.length; i++)
     {
      var nom = params[i];  // Recuperwem el nom del parametre de la llista params 
      var valor = dades[nom]; // Recuperem el valor del parametre 
      frase = frase + nom + " = " + valor + " \n";  // Anem afegint paramteres a la frase 
     
    }
    
   sendText(id,frase);   // Enviem les dades recuperades 
     
     
}


