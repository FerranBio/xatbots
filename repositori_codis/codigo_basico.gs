/*
Este código se puede considerar una plantilla básica para crear el primer xatbot
Los pasos a seguir serían:

1.- Registrar el xatbot a BotFather de telegramas. Puede aprovechar y configurar con las 3 pedidos que tiene programadas info, random y me.
2.- Crear un documento Google Apps Script o una Hoja de Cálculo en Drive
3.- Abrir el espacio de código y pegar este código que tiene aquí
4.- Personalice variables iniciales token y SSID si utiliza hoja de cálculo
Posteriormente una vez
5.- Publicar este código y obtendrá la url de tu applicació que puede llenar la variable webAppUrl
6.- Inicia tu url telegramas utilizando la sintaxis:
https://api.telegram.org/botAPI_KEY/setWebhook?url=url_WebAppUrl
7.- Finalmente si todo ha ido bien ya tendrá operativo su xatbot

*/ 


var token = ""; // Api token de su bote que obtendrá al BotFather
var telegramUrl = "https://api.telegram.org/bot" + token;
var webAppUrl = ""; // Url del script de Google
var ssId = ""; // Id de la hoja de cálculo que está trabajando

// Plantilla para regisrar nuestra url en Telegram 
// Debes substituir API_KEY por token y url_WebAppUrl  por webAppUrl

//https://api.telegram.org/botAPI_KEY/setWebhook?url=url_WebAppUrl


// [1] Funciones  para enviar contenidos a Telegram 

function sendPhoto(id,foto,caption)
{
var url = telegramUrl + "/sendPhoto?chat_id=" + id + "&photo=" + foto+"&caption=" + caption ;
 
  var response = UrlFetchApp.fetch(url);
  Logger.log(response.getContentText());
}

function sendAudio(id,audio,caption)
{
var url = telegramUrl + "/sendAudio?chat_id=" + id + "&audio=" + audio+"&caption=" + caption ;
 
  var response = UrlFetchApp.fetch(url);
  Logger.log(response.getContentText());
}




function sendText(chatId,text,keyBoard){
  keyBoard = keyBoard || 0;

 
  if(keyBoard.inline_keyboard || keyBoard.keyboard){
     var data = {
      method: "post",
      payload: {
         method: "sendMessage",
         chat_id: String(chatId),
         text: text,
         parse_mode: "HTML",
         reply_markup: JSON.stringify(keyBoard)
       }
     }
    }else{
      var data = {
        method: "post",
        payload: {
          method: "sendMessage",
          chat_id: String(chatId),
          text: text,
          parse_mode: "HTML"
        }
      }
    }

   UrlFetchApp.fetch( telegramUrl + '/', data);

 }

/* Codigos HTML permitidos en los mensajes de Telegram 

<b> negreta </b>, <strong> negreta </strong>
<i> cursiva </i>, <em> cursiva </em>
<u> subratllar </u>, <ins> subratllar </ins>
<s> strikethrough </s>, <strike> strikethrough </strike>, <del> strikethrough </del>
<b> negreta <i> cursiva negreta <s> cursiva en negreta en cursiva </s> <u> subratlla en negreta en cursiva </u> </i> negreta </b>
<a href="http://www.example.com/"> URL en l�nia </a>
<a href="tg://user?id=123456789"> esment en l�nia d'un usuari </a>
<code> codi d'amplada fixa en línia </code>
<pre> Bloc de codi de l'amplada fixa preformatat </pre>
<pre> <code class = "language-python"> bloc de codi preformatat d'amplada fixa escrit en el llenguatge de programació de Python </code> </pre>

*/


function sendDocument(id,fitxer)
{
var url = telegramUrl + "/sendDocument?chat_id=" + id + "&document=" + fitxer;
var response = UrlFetchApp.fetch(url);
Logger.log(response.getContentText());
}

function sendVideo(id,fitxer)
{
var url = telegramUrl + "/sendVideo?chat_id=" + id + "&video=" + fitxer;
var response = UrlFetchApp.fetch(url);
Logger.log(response.getContentText());
}

function sendLocation(id,lat,long)
{
var url = telegramUrl + "/sendlocation?chat_id=" + id + "&latitude=" + lat + "&longitude=" + long ;
var response = UrlFetchApp.fetch(url);
Logger.log(response.getContentText());
}


function sendDocument2(chatId,id,caption){

  var fileId = id ;
  var img = DriveApp.getFileById(id);  
  var blob2 = img.getBlob().getAs("text/plain");
 

  var payload = {
          method: "sendDocument",
          chat_id: String(chatId),
          document: blob2,
          caption : caption,
          parse_mode: "HTML"
          //disable_web_page_preview: true,
  };
 
  var options = {
    method: "POST",
    payload: payload,
    muteHttpExceptions : true
  };
     
   var request = UrlFetchApp.fetch( telegramUrl + '/', options);
   Logger.log(request.getContentText());
 }



function deleteMessage(id,id_missatge)
{
var url = telegramUrl + "/deleteMessage?chat_id=" + id + "&message_id=" + id_missatge;
var response = UrlFetchApp.fetch(url);
Logger.log(response.getContentText());
}


function getFile(file_id) {
  var url = telegramUrl + "/getFile?file_id=A...EC";
  var response = UrlFetchApp.fetch(url);
   Logger.log(response.getContentText());
}


function sendText3(id,text)
{
//var url = telegramUrl + "/sendMessage?chat_id=" + id + "&text=" +text +"&parse_mode=html" ;
 url = telegramUrl + "/sendMessage?chat_id=" + id + "&text=" +text ;
var response = UrlFetchApp.fetch(url);
Logger.log(response.getContentText());
}

function sendPhoto3(chatId,blob2,caption){

  var payload = {
          method: "sendPhoto",
          chat_id: String(chatId),
          photo: blob2,
          caption : caption,
          parse_mode: "HTML"
    
          //disable_web_page_preview: true,
  };
 
  var options = {
    method: "POST",
    payload: payload,
    muteHttpExceptions : true
  };
     
   var request = UrlFetchApp.fetch( telegramUrl + '/', options);
   Logger.log(request.getContentText());
 }


function downloadFile(fileURL,folder) {
  
  var fileName = "";
  var fileSize = 0;
  
  var response = UrlFetchApp.fetch(fileURL, {muteHttpExceptions: true});
  var rc = response.getResponseCode();
  
  if (rc == 200) {
    var fileBlob = response.getBlob()
    var folder = DocsList.getFolder(folder);
    if (folder != null) {
      var file = folder.createFile(fileBlob);
      fileName = file.getName();
      fileSize = file.getSize();
    }
  }
    
  sendText(id,"Gravado");
}


// Fin de las funciones de Telegram [/1] 

// [2] función doPost que recibe datos externos por metodo POST tal como los envia Telegram 

function doPost(e) {
  
  var data = JSON.parse(e.postData.contents); // Asigna los datos pasados per Telegram en formato JSON a una variable data 

//MailApp.sendEmail('email', "titulo" ,JSON.stringify(data,null,4) ) ;   
//Logger.log(data)  ; 

// Telegram utiliza variables distintas para cada tipo de dato 

// Para contenidos de tipo mensajes de texto 
try{

if(data.message)  // En cas de que no fem servir callback 
{
  
var text = data.message.text;  // Recupera el text del missatge 
var id = data.message.chat.id;  // Recupera el id de la finestra d'on procedeix el missatge 
var id_usuari = data.message.from.id; // Recupera el id de l'usuari que ha escrit el missatge 
var id_missatge = data.message.message_id; // Recupera el id del missatge
var update_id =   data.update_id; // Recupera el id del missatge final 
var lang = data.message.from.language_code ;  // Recupera l'idioma que te el Telegram de l'usuari que ha enviat el missatge 
var nom = data.message.from.first_name ;  // Recupera tot el nom de l'usuari que ha enviat el missatge 
var location = data.message.location; 
}
}
catch(err){
    Logger.log(err); //
  }

// fin de contenidos de mensajes de texto 


// Para contenidos de botones 

try{
  
if(data.callback_query)
  {
  var id_usuari = data.callback_query.from.id; 
  var id = data.callback_query.message.chat.id;   
  var id_missatge = data.callback_query.message.message_id; // Recupera el id del missatge
  var text = data.callback_query.data;
  var usuari =  data.callback_query.from.user_name; 
  var nom =  data.callback_query.from.first_name; 
  var lang =  data.callback_query.from.language_code; 
 }  
  }
catch(err){
    Logger.log(err); //
  }


// fin de recojida de datos de botones 






var idioma = lang; 
var enviat = false; 
  
  
  
var entrada = text.split('@');  // Separa las palabras entradas en una matriz/array y asi obtener por un lado  el comando y del otro los valores  
var comanda = entrada[0]; //El comando sera la primera palabra empieza a contar desde cero 

var comanda0 = comanda.split(' '); // Separa el comando de los parametros que contenga  
var comanda = comanda0[0];      // El comando queda`rá ala izquierda en la posiciópn cerpo de la lista 
  
  sendText(id,comanda); 
  
switch(comanda) 
      {
        case '/start'  :   // Fabrica el mensaje inicial del bot 
             var resposta = 'Bienvenido/Bienvenida al xatbot '; 
        break;
          
        case '/info':  // Muestra información 
             var enviat = info(id_usuari,idioma); 
        break;  
        
        case '/idioma':  // Muestra botones para seleccionar idioma
             var enviat = menu_idioma(id_usuari,idioma); 
        break;  
                      
        case '/random':  // Muestra un avatar aleatorio
             var enviat = avatar(id_usuari,idioma); 
        break;  
                              
        case  '/me' : // Muestra la propia información en Telegram 
           var enviat = me(id_usuari,nom,idioma) ; 
          break; 
          
        default   :  // Ejecuta esta opción cuando no encuentra el comando entre las opciones 
          var resposta = "No entiendo que me pides" + text  ;
             break;
    }

  if(enviat != true) sendText(id,escribe_frase(resposta,idioma));  // Envia resposta 

}


// Fin de la función doPost [/2] 


// [3] Espacio para las funciones del bot que gestionaran las respuestas que se deben tramitar 


function info(id,idioma)
{

var frase = "Este Xatbot  tiene como objectivo ayudarte en el desarrollo de tu proyecto " + 
             "Puedes acceder a los comandos del xatbot pulandosobre la '/' y seleccionando la opción deseada \n\n "+ 
             "Los comandos que puedes activar  directamente son:  \n\n" +  
             "/info -  Información  \n"+
             "/random - Avatar aleatòrio \n" + 
             "/idioma - Selecciona idioma \n "+ 
             "/me - Mis datos  \n " ;  
             
             
  

 sendText(id,escribe_frase(frase,idioma));   
   
  return true; 
     
}





function escribe_frase(frase,idioma)
{

  if(idioma.length ==2 && idioma !="es") var frase= LanguageApp.translate(frase, 'es', idioma); // Si los dos idiomas coinciden da error por eso excluimos el castellano 
 
  return frase ; 
  
}


function menu_idioma(id,idioma)
{
 
 
 
 var llista = new Array(
   [{"text": escribe_frase("Castellano",idioma), "callback_data": "/idi es"},
    {'text': escribe_frase("Catalan",idioma), "callback_data" : "/idi ca"},
    {"text": escribe_frase("Vasco",idioma),"callback_data" : "/idi eu"}],
   [{"text": escribe_frase("Inglés",idioma),"callback_data" : "/idi en"},
    {"text": escribe_frase("Francés",idioma),"callback_data" : "/idi fr"},
    {"text": escribe_frase("Gallego",idioma),"callback_data" : "/idi gl"}],
   [{"text": escribe_frase("Àrabe",idioma), "callback_data" : "/idi ar"},
    {"text": "Tancar","callback_data" : "/tancar"}]) ; 
  
  var tecles =  { inline_keyboard: llista    , resize_keyboard: true,one_time_keyboard : true  }; 
  
  sendText(id,escribe_frase("Selecciona idioma",idioma),tecles );  

  return true; 
  
}

 
function avatar(id_usuari)
{
  //https://picsum.photos/200 alternativa a robohash 
  
var lista_palabras = new Array("computer","chair","water","glass","dog","mouse","bus" ); 
var palabra = lista_palabras[Math.floor(Math.random() * lista_palabras.length)];                           
 var url = "https://robohash.org/" + palabra ;  // Creamos la url del generador de avatares a partir de una palabra
  
  sendPhoto(id_usuari,url,palabra);  // Enviamos la imagen a Telegram 
         
           
  var llista = new Array([{"text" : "Si", "callback_data": "1" } , {"text" : "No", "callback_data": "2"}]) ; // Lista de las dos opciones de votación 
               var tecles =  { inline_keyboard : llista    , resize_keyboard: true,one_time_keyboard : true  };  // Asigna la lista de botones al comando  keyboard 
               sendText(id_usuari,"¿Te gusta la imagen? ",tecles );  // Envia un mensaje a Telegram usando la función sendText()  

  
  return true;  // Devuelve cierto para evitar que se envie un mensaje extra 
}



function me(id_usuari,nom,idioma)
{
 
  sendText(id_usuari," Tu nombre en Telegram és : " + nom + " utilizas en Telegram el idioma  " + idioma ); 
  
  return true; 
  
} 

   
 




