/* 
Nom de l'script = registre.gs
Autor = @FerranMas  / profefp.tk@gmail.com 
Data primera versió = 16/09/2021

Aquest codi permet gestionar el registre d'usuaris en un grup de Telegram. De manera que amb la comanda /registre email, analitzará si el usuari esta registrat o no, i si no está registrat envia un misssatge d'autorització que si es prem el botó de "Si" executa una comanda de gravació que assigna el id de Telegram al correu indicat per l'úsuari. 

En primer lloc la funció registrar analitza si s'ha enviat correu i si aquest usuari ja esta registrat. En cas de no estar registrat genera un missatge d'autorització amb dos botons "Si" i "No"   que incorporen la comanda /reg -1-nom@correu.cat que s'analitza en la funció reg 

*/


// Caldrà afegir la comanda "/registre" i tambe "/reg"  dins del switch 

switch(comanda) 
{
  
...
    
         case '/registrar' : 
           var enviat = registrar(id_usuari,nom,text,idioma); 
           break; 

         case '/reg' : 
           var enviat = reg(id_usuari,usuari,nom,text,idioma) ;  
           break; 
           
             
... 


}


// La funció registrar ecull la petició de registrar i analitza si s'ha enviat correu i si existeix el registre de l'usuari i ens porta a un missatge d'autorització que controlem amb la funció reg 

function registrar(id_usuari,nom,text,idioma) 
{
   
 var em = text.split(" "); // Per obtenir el parametre de la dreta de /registrar
 var mail = em[1];  // Ens obtindrà l'email de la comanda 
 
 if (mail.length < 5 ) // Controlem si la longitud de l'email es més petita de 5 
     {
       sendText(id_usuari,"No has indicat email o és massa curt"); 
       return true; 
     }
       
       var dades = SpreadsheetApp.openById(ssId).getSheetByName("Registre").getDataRange().getValues();  // Carreguem totes les dades del full de Registre d´usuaris
         
   for(var i = 0; i<dades.length;i++){
       if(dades[i][1] == id_usuari) 
       { //Cerca si el mail esta registrat 
      sendText(id_usuari,"Ja estas registrat"); 
      return true; 
    }  
  }
  
 // Si no està registrat genera el missatge d'autorització  
  var llista = new Array([{"text": "SI", "callback_data": "/reg -1-"+ mail},
                         {"text": "NO", "callback_data" : "/reg -0-" + mail}]) ; 
  
               var tecles =  { inline_keyboard: llista    , resize_keyboard: true,one_time_keyboard : true  }; 
 
  sendText(id_usuari,"Si vols que es registri l'email del teu usuari i dones permis per a emprar el registre en les activitats del grup , prem 'SI' en cas contrari prem `No' ",tecles); 
  return true; 

  }
  

  // La funció reg rep la esposta de l'autorització i segons sigui Si , gravarà les dades al full de registre o si es No ho fara 

  function reg(id_usuari,usuari,nom,text,idioma)
  {
    
  var em = text.split("-"); // Per separar parametres /reg -1-nom@correu.cat
  var opcio= em[1]; // Separa l'opció 1 o 0 
  var mail = em[2]; // Separa el mail 
  
  
  if(opcio=="1") // Si ha escollit "Si" com autorització 
  {
  SpreadsheetApp.openById(ssId).getSheetByName("Registre").appendRow([new Date(),id_usuari,mail,nom,usuari,idioma]);  // Afegueix una fila amb les dades de l'usuari registrat 
  sendText(id_usuari,"Registre completat"); 
   }
   else sendText(id_usuari,"Registre cancel·lat"); 
   
  
  return true ; 
  }
  
  
