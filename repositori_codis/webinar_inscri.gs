/* 
Nom de l'script = webinar_inscri.gs
Autor = @FerranMas  / profefp.tk@gmail.com 
Data primera versió = 30/08/2020

Aquest codi permet gestionar l'inscripció dels usuaris a una webinar creat amb el Meet, de manera que la gestió conmpren: 
1.- Gravació dels inscrits en el full de calcul Drive 
2.- Envia missatge de confirmació de la inscripció que cal validar clicant a l'enllaç que porta un codi. Aquesta validació es fa des de doGet 
3.- Envia alta de l'úsuari inscrit a l'event Meet del calendari, l'úsuari rebrà el missatge de Google Meet amb les dades de connexió al webinar 

*/


/*
 El doGet el farem servir per validar el missatge que s'ha enviat a l'úsuari per assegurar la validessa del correu que ens ha indicat rebrem tres paràmetres accio que serà "validar" ,la fila el numero de fila del full i la clau 
*/


function doGet(e)
{
  var accio = e.parameter.accio; 
  var id = e.parameter.id; 
  var clau = e.parameter.clau; 
 
var sortida = "Error" ;   
switch (accio)
      {
        case 'validar'  :   // Valida l'inscripció al webinar 
        var sortida = valida_inscri(id,clau);  // funció per validar l'inscripció 
        break; 
          
      }
  
  return HtmlService.createHtmlOutput(sortida); // retorna via web el missatge de validació 
}

.... 

// Caldrà afegir la comanda "/inscri"  dins del switch 

switch(comanda) 
{
  
...

case '/inscri': 
              var enviat = inscri(id_usuari,nom,text,idioma); 
         break; 

... 
}


/* 
En primer lloc la funció inscri ens recull la petició d'inscripció i cal que la comanda /inscri estigui acompanyada d'ún email per fer el registre : sigui "/inscri email"   
*/

function inscri(id,nom,text,idioma) 
{
 
 //Si no tenim cap webinar actiu activem el missatge i retornem 
 // sendText(id,escriu_frase("Actualment no hi ha cap inscripció oberta",idioma)); 
 // return true; 
 
 var nom_webinar = "XXXXXXXXXXXXX"; // Millor declarar a la capçalera 
 var full_webinar = "webinar0";  // Millor declarar a la capçalera o a partir d'una llista de posibles webinars 
 
 
 var ssId = ""; // L'Id del full que el podem tenir definit globalment al principi 
 var num_max = 50;  // Nombre màxim de participants port estar també a la capçalera 

 var em = text.split(" "); // Per obtenir el parametre que acompanyara al /inscri normalment serà el email però pot ser "anular" estarà com a paràmetre a la dreta de /inscri 
 var param = em[1]; // Recollim el paràmetre a la dreta de l'espai com element segon serà el 1

 var mail = param ;  // El parametre més emprat serà el mail per això fem servir aquest nom 
  
  var sh = SpreadsheetApp.openById(ssId).getSheetByName(full_webinar);  // Connectem amb el full 
  var dades = sh.getDataRange().getValues();  // Carreguem les dades per identificar el usuari 
  var num = sh.getLastRow();         ;  // identifiquem el numero d'ínscripció darrer
 
   
  var trobat = 0;
 
      
  for(i in dades)  // Fem un bucle per localitzar l'usuari per si estes ja inscrit 
  {
 
    var row = dades[i]; // Assigem la fila "i" a row 
    var num_usuari = row[1]; // Recollim el id d'úsuari
    
    if(num_usuari == id)  // identifiquem si l'usuari ja està inscrit 
    {
      var mail = row[3]; // agafem el correu 
      if(param=="anular"){   // Si està inscrit i el paràmetre és anular, l'esborrarem 
           var fila = +i+1; // ajustem l'index perquè comença a comptar per zero 
           
           SpreadsheetApp.openById(ssId).getSheetByName(full_webinar).deleteRow(fila);  // Anul.lem la fila de l'usuari 
           sendText(id,escriu_frase("S'ha anul·lat la teva inscripció al Webinar" + nom_webinar,idioma)); 
        
          // Generem el missatge d'anul.lació de l'inscripció  
          var subject = "Anul·lació Inscripció al Webinar " + nom_webinar ;
 
       
          var plain_email_body = "Has cancel.lat teva inscripció al Webinar sobre '" + nom_webinar +"', \n" +
                                  " Esperem pugis participar en altres webinars \n" + 
                                  " Salutacions \n\n\n" +
                                  "L'Admin \n\n"; 

           var html_body =      "Has cancel.lat teva inscripció al Webinar sobre '"+ nom_webinar +"', <br />" +
                                  " Esperem pugis participar en altres webinars <br />" + 
                                  " Salutacions <br /><br /><br />" +
                                  "L'Admin <br/><br/>"; 


          var advancedOpts = { name: "Cancel.lació inscripció Webinar " + nom_webinar , htmlBody: html_body };

             
         MailApp.sendEmail(mail, "Cancel.lació Inscripció Webinar" + nom_webinar  ,plain_email_body,advancedOpts) ;  //Envia Mail de cancelació de l'inscripció
  
          sendText(id,escriu_frase("Rebràs un correu confirmant la cancel.lació al Webinar " + nom_webinar,idioma));  // Envia el missatge de cancel.lació per Telegram 
          afegir_event(mail,"eliminar"); 
        
        return true; 
        }
 
 
// Si localitza l'usuari i el parametre no es anular, li oferim la possibilitat d'anular        
      sendText(id,escriu_frase("Ja estas inscrit al Webinar " + nom_webinar,idioma)); 
              var llista = new Array([{"text" : escriu_frase("Anul·lar inscripció",idioma) , "callback_data": "/inscri anular" }]) ; 
              var tecles =  { inline_keyboard : llista    , resize_keyboard: true,one_time_keyboard : true  };  // Assigna la llista de botons a la  comanda keyboard 
               sendText(id, escriu_frase ("Vols anul·lar la inscripció? ",idioma),tecles );  // Envia un missatge a Telegram amb botons 
      return true; 
          
    }
    
  }
  

    
  if(param === undefined)  // Per si no està inscrit i no ha indicat email 
  {
   sendText(id,escriu_frase("Per formalitzar la teva inscripció escriu /inscri seguit del teu email",idioma)); 
   return true;
   }
  

 
  if(num > num_max) // Controlem el nombre màxim d'inscrits 
 {
   sendText(id,escriu_frase("Inscripció tancada ",idioma)); 
   return true; 
   
 }  
   


// url de validació =   
  var clau = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!$*-";

// generem la clau al.leatoria  
 
  for(var j=0;j<8;j++)
  {
     clau += possible.charAt(Math.floor(Math.random() * possible.length));
   
  }
   
  // agafarem la url del nostre script i li afegirem els 3 paràmetres : accio,id i clau 
  var url_valida =  webAppUrl+"?accio=validar&id="+num+"&clau="+clau; 


// Afegim les dades de l'usuari inscrit al full d'inscripcions del webinar 
var idioma = idioma || "ca" ; 
  
var dades = SpreadsheetApp.openById(ssId).getSheetByName(full_webinar).appendRow([new Date(),id,nom,param,idioma,clau]);  
  
  
  
// Preparem el missatge de inscripció amb imatge banner de l'event   
  
 var id_banner = "" ; // Id de l'imatge del webinar pujada al Drive 
 var url_foto = "https://drive.google.com/uc?export=view&id="+id_banner ;  // Url d'una imatge a partir de id de Drive 
    
 // Assignem imatge a variable de tipus Blob 
  var img = UrlFetchApp
                       .fetch(url_foto)
                       .getBlob()
                       .setName("Webinar");    

  

  var subject = "Inscripció al Webinar " + nom_webinar  ;

  
  
var plain_email_body = "Moltes Gracies " + nom +", \n\n " +
                       "Per iniciar el procés de la teva inscripció al Webinar '" + nom_webinar + "',\n" +
                       "Que tindrà lloc el dilluns dia XX de YY a les 16h.   \n " +
                       "Per formalitzar la teva inscripció has de validar el procés accedint al seguent enllaç : \n" +
                        url_valida + "  \n\n\n "+     
                       "L'Admin \n\n"; 

var html_body =       " Moltes Gracies  <b>" + nom +"</b>  <br /><br />" +
                       "Per iniciar el procés de la teva inscripció al Webinar <i><b>'" + nom_webinar +"'</b></i> <br/>" +
                       "Que tindrà lloc el dilluns dia XX de YY a les 16h.   <br/> " +
                       "Per formalitzar la teva inscripció has de validar el procés "+
                         "<a href='"+ url_valida + "'><b>clicant aquí </b></a> o accedint al seguent enllaç : <br />" +
                        url_valida + "  <br /><br />"+     
                       "L'Admin <br/><br/>"; 

var advancedOpts = { name: "Inscripció Webinar Xatbot de Telegram ", htmlBody: html_body,body: img,attachments: img};

   
MailApp.sendEmail(mail, "Inscripció Webinar" ,plain_email_body,advancedOpts ) ;   // Enviem mail d'inscripció 
  
  
  
  var tros1 = escriu_frase("Moltes gracies ",idioma); 
  var tros2 = escriu_frase(" per la teva inscripció al webinar " + nom_webinar +" \n",idioma); 
  var tros3 = escriu_frase("Rebràs un correu perque confirmis la teva inscripció i amb informació que et pot resultar d'interès. ",idioma); 

  sendText(id, tros1 + nom + tros2 + tros3 ); 

return true; 

    }


/* Aquesta segona funció serveix per validar l'email del participant que s'ha inscrit 
En clicar l'enllaç entrarà a l'script per la funció doGet que el portarà fins a la funció valida_inscri enviant dos parametres la fila i la clau 
*/


function valida_inscri(fila,clau)
{
  

var ssId = "";  // Id del full de càlcul que normalment tindrem a la capçalera 
var nom_webinar = "" ;  // Nom del webinar que podem afegir a la capçalera o obtenir per altres sistemes si en tenim més d'úna opció 

  var dades = SpreadsheetApp.openById(ssId).getSheetByName(full_webinar).getDataRange().getValues();  // Carreguem les dades per identificar el usuari 
  var row = dades[fila]; // anem directament a la fila 
  var id_telegram = row[1]; 
  var nom = row[2]; 
  var mail = row[3]; 
  var idioma = row[4]; 
  var password = row[5]; 
  
  // Comprovem si la clau es la correcta 
  if(clau != password) { 
        return escriu_frase("Error en la clau de validació",idioma); 
  }
  
  SpreadsheetApp.openById(ssId).getSheetByName(full_webinar).getRange(+fila+1,7).setValue(new Date());   // Validem l'inscripció garvant la data 

// Preparem missatge de mail de confirmació 
  
  var plain_email_body = "Benvolgut/a " + nom + "\n , la teva inscripció al Webinar '" + nom_webinar +"' , s'ha validat \n" +
                    "Properament rebràs l'enllaç de connexió de la sessió del Meet on es farà el Taller \n\n\n" +
                    "L'admin \n\n"; 

var html_body =       "Benvolgut/a <b>" + nom  + "</b> , <br /> la teva inscripció al Webinar '" + nom_webinar +"' , s'ha validat <br />" +
                      "Properament rebràs l'enllaç de connexió de la sessió del Meet on es farà el Taller <br/> <br /> <br />" +
                       "Ferran <br /> <br />"; 


  var advancedOpts = { name: "Confirmació Inscripció Webinar " + nom_webinar, htmlBody: html_body} ;
  
   MailApp.sendEmail(mail, "Inscripció Webinar" + nom_webinar ,plain_email_body,advancedOpts ) ;   // Enviem mail de confirmació inscripció

// Afegim l'email de l'usuari a l'event Meet del calendari 
   afegir_event(mail,"afegir"); 

  // sendText(id_telegram, escriu_frase("La teva inscripció al webinar " + nom_webinar +"  s'ha validat, podràs obtenir més informació amb la comanda /webinar ",idioma));  // Envia missatge al telegram privat     
  
  return escriu_frase("La teva inscripció al Webinar " + nom_webinar +" ha estat validada , trobaràs informació del webinar amb la comanda /webinar al xatbot del grup ",idioma); 
  
}



/* 
Finalment la funció afegir_event() , ens permet accedir via APi al Google Calendar i afegir l'email de l'úsuari que serà invitat a un event de Meet 
D'aquesta manera el mateix Google Meet ha enviara el missatge a l'úsuari amb la informació del webinar data i codi de connexió 
*/


function afegir_event(mail,accio)
{


var idCal = "XXXXX@group.calendar.google.com"; // Id del calendari que te l'event del webionar 

 // https://www.base64decode.org/  per codificar el event, si ho fem manualment , però en aquest cas ho farem automaticament , partin de l'id de l'event 

// Aneu a l'event i l'editeu, a la url del navegador podereui obtenir el id de l'event 
// Caldrà transformar aquest id de l'event 
var id_event = "NDdhaHN1dnM5aGZmxxxxxxmJqNzU5djkgOHN1a3RtMmtpdW1qNGxvcDxxxxxxxNGtAZw" ; 

// Procés per tyransformar el id del event en un codi de l'event a format base64 
var decoded = Utilities.base64Decode(id_event);
var session = Utilities.newBlob(decoded).getDataAsString();   
var id_ev = session.split(" ");   
var id_event = id_ev[0]  ; 
  
  
var event = Calendar.Events.get(idCal, id_event); // identifiquem l'event 
  
//El que fa el procés és recuperar la llista d'emails dels invitats i afegir o eliminar 

if(accio=="afegir")
{  
  if(event.attendees) {
    var primer = event.attendees[0].email; 
        event.attendees.push({
      email: mail
    });
  } else {
    event.attendees = new Array({email: mail});
  }
  }
  
  
if(accio=="eliminar")
{  
var llista = ""; 
if(event.attendees) {
  var i  =0; 
  while(i < event.attendees.length) 
     {
      llista = event.attendees[i].email; 
      
      if(event.attendees[i].email == mail) 
      {
         event.attendees.splice(i,1); 
       }
       i++; 
      }
 
  }
  
  }
  
  
  
event = Calendar.Events.patch(event, idCal,  id_event, {
  sendUpdates: "all" });  
  
}
 


