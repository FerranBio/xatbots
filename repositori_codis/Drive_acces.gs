/* 
Nom de l'script = Drive_acces.gs 
Autor = @FerranMas  / profefp.tk@gmail.com 
Data primera versió = 17/08/2020

Aquest codi permet accedir des de Telegram a una carpeta del Drive 
Fa servir botons per mostrar i escollir el document a descarregar 
La gestió de la carpeta (back-end)  es pot fer des del propi Drive per diferents usuaris que la comparteixin 
*/




// Entra per la funció drive i aquesta pot incloure el id de la carpeta si es tracta d'una subcarpeta , si no porta id agafa el id de la carpeta inicial . 

function drive(id,text,idioma)
{

var folderid = ""  ;   // Heu d'indicar el Id de la carpeta a compartir 
var fol = text.split(" "); // Separa la comanda de l'id de la carpeta 
var ID_carpeta = fol[1]; // Agafa el ID de la carpeta del Drive 
var folderId = ID_carpeta || folderId  ;  // Si no porta ID vol dir que hem d'agafar el de la carpeta inicial 


var parentFolder = DriveApp.getFolderById(folderId); // Creem l'objecte carpeta 
getChildFolders(parentFolder,id,idioma); // Cridem a la funció que analitza la carpeta 

}


function getChildFolders(parent,id,idioma) {
  
  var childFolders = parent.getFolders();
  var llista = new Array(); 
  
  var arrel = parent.getParents();  // Per conìexer la carpeta de procedència per retornar 
  var arrel_id = arrel.next().getId(); 
  
  
  // Per analitzar si és l'arrel que ja no te els ".." de Retorn "  ; 
  if(parent.getId()== folderId) llista.push([{'text': "📂" + parent.getName(), 'callback_data': '/drive ' + parent.getId()}]);
  else 
  {
  // si no és l'arrel afegim ".." per retornar endarrera  
  llista.push([{'text': "📂" + parent.getName(), 'callback_data': '/drive ' + parent.getId()},
                    {'text': ".." , 'callback_data': '/drive ' + arrel_id  }]); 
                     
    }
 
  
  // Primer cercarem les carpetes
  
  while (childFolders.hasNext()) {
  
    var childFolder = childFolders.next();
    llista.push([{'text': "📂 " + childFolder.getName(), 'callback_data': '/drive ' + childFolder.getId() }]); 
   
   
   }

 var emoji_audio = "🎧" ; 
 var emoji_texto = "📋"; 
 var emoji_imagen = "🌆"; 
 var emoji_video = "🎥"; 
 var emoji_undefined = "💾"; 
 var emoji_tutorial = "📗"; 
    
  // Despres mostrarem els fitxers de la carpeta actual 
     var files = parent.getFiles();

    while (files.hasNext()) {
          
      file = files.next();
      nom = file.getName();
      url = file.getUrl();
      tipus = file.getMimeType();
      
    if(tipus.indexOf('image') > -1) var emoji = emoji_imagen; 
    if(tipus.indexOf('text') > -1) var emoji = emoji_texto; 
    if(tipus.indexOf('ogg') > -1) var emoji = emoji_audio; 
    if(tipus.indexOf('pdf') > -1) var emoji = emoji_tutorial; 
       
          
      llista.push([{'text': emoji + " " + nom, 'url':  url }]);
  
    }

                var tecles =  { inline_keyboard: llista    , resize_keyboard: true,one_time_keyboard : true  }; 
               sendText(id,escriu_frase("Fitxers del Drive ",idioma),tecles );  
               return true ; 

}

