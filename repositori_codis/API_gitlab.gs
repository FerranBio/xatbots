/*
Aquest script connecta amb la carpeta repositori_codis del projecte Xatbots de Gitlab 
https://gitlab.com/FerranBio/xatbots/-/tree/master/repositori_codis 
Farem servir l'API de GitLab .

Amb la funció Gitlab(id) que cridarem primer accedirem a la llista de fitrxers de la carpeta escollida fent servir la comanda de l'API    
https://gitlab.com/api/v4/projects/" + id_repo + "/repository/tree?recursive=true"; 
Aquesta petició REST API ens retorna en format JSON la llista de fitxers de la carpeta 
Amb aquesta llista generem botons inline_keyboard per accedir a cada fitxer a partir del nom 

Amb la funció git(id_usuari,text) farem un acces a l'API via curl per carregar el fitxer escollit i omplirem una variable amb format BLOB i despres enviarem com a document de Telegram fent servr el format blob  
Fem servir l'API per accedir a un fitxer pel seu nom 
https://gitlab.com/api/v4/projects/ + id_repo + /repository/files/ + carpeta_git + nom_fitxer + /raw?ref=master"  
*/



// Inici del codi 
// Variables globals 

var id_repo = ""; // el ide del nostre repositori 



// Agafa el document com a variable blob 

function sendDocument3(chatId,blob2,caption){

   var payload = {
          method: "sendDocument",
          chat_id: String(chatId),
          document: blob2,
          caption : caption,
          parse_mode: "HTML"
          //disable_web_page_preview: true,
  };
 
  var options = {
    method: "POST",
    payload: payload,
    muteHttpExceptions : true
  };
     
   var request = UrlFetchApp.fetch( telegramUrl + '/', options);
   Logger.log(request.getContentText());
 }


function doPOst(e) 
{
var data = JSON.parse(e.postData.contents); // Assigna les dades pasades per Telegram en format JSON a una variable data 
//MailApp.sendEmail(mail, "Gitlab" ,JSON.stringify(data,null,4) ) ;  

  try{  
if(data.message)  // En cas de que no fem servir callback 
{
var text = data.message.text;  // Recupera el text del missatge 
var id = data.message.chat.id;  // Recupera el id de la finestra d'on procedeix el missatge 
var id_usuari = data.message.from.id; // Recupera el id de l'usuari que ha escrit el missatge 
var id_missatge = data.message.message_id; // Recupera el id del missatge
var update_id =   data.update_id; // Recupera el id del missatge final 
var lang = data.message.from.language_code ;  // Recupera l'idioma que te el Telegram de l'usuari que ha enviat el missatge 
var nom = data.message.from.first_name ;  // Recupera tot el nom de l'usuari que ha enviat el missatge 
var location = data.message.location; 
}
}
  catch(err){
    Logger.log(err); 
  }

  
try{  
 if(data.callback_query)
  {
  var id_usuari = data.callback_query.from.id; 
  var id = data.callback_query.message.chat.id;   
  var id_missatge = data.callback_query.message.message_id; // Recupera el id del missatge
  var text = data.callback_query.data;
  var usuari =  data.callback_query.from.user_name; 
  var nom =  data.callback_query.from.first_name; 
  var lang =  data.callback_query.from.language_code; 
  }
  }
  catch(err){
    Logger.log(err); 
  }  


// al switch .... 


 case '/gitlab' :  
        case '/GitLab':     // Per mostrar els fitxers a la carpeta repositori 
            var enviat = Gitlab(id_usuari); 
          break; 
          
        case '/git':  // Per descarregar el fitxer escollit 
            var enviat = git(id_usuari,text); 
          break; 





function Gitlab(id_usuari)
{
   var id_repo = ""; // id de la vostra carpeta repositori 
   var carpeta_git = "repositori_codis" + "%2F"; // Ha d'estar encoded per això afegim el codi de la /  
   var url_repo = "https://gitlab.com/api/v4/projects/" + id_repo + "/repository/tree?recursive=true"; 
 

  var response_repo = UrlFetchApp.fetch(url_repo); 
  var json = response_repo.getContentText();      
  var dades = JSON.parse(json);       //Parsejem, que consistirà en convertir el resultat a files i columnes
 
  
  // Per recuperar les dades haurem de fer servir els noms i les rutes que podem veure en el quadre anterior  

 // Llegim les dades que ens interessen

 var fila = new Array(); 
   
   for(i in dades)
  {
  var id = dades[i].id;    
  var name = dades[i].name; 
  var type = dades[i].type;     
  var path = dades[i].path;     
  var mode = dades[i].mode; 

  if(mode =="100644" && name != ".gitkeep" ) // Nomes escollim els fitxers de text pla 
  {
    
   
           fila.push([{'text': name , 'callback_data':  '/git ' + name  }]);
    }
  }
  
         var tecles =  { inline_keyboard: fila    , resize_keyboard: true,one_time_keyboard : true  }; 
           sendText(id_usuari,"Fitxers GitLab", tecles );    
    
return true;                    
   }
  
 
 
function git(id_usuari,text)
{
 
  
  
var nom = text.split(" "); 
var name = nom[1];   

   var id_repo = ""; // el ide del nostre repositori pot estar declarat global 
   var carpeta_git = "repositori_codis" + "%2F"; // Ha d'estar encoded per això afegim el codi de la /  
   var url = "https://gitlab.com/api/v4/projects/" + id_repo + "/repository/files/" + carpeta_git + name + "/raw?ref=master"; 
 
       var response = UrlFetchApp.fetch(url);
       var binaryData = response.getContent();

        
         var blob = Utilities.newBlob(binaryData,'plain/text',name);  
           
    
  sendDocument3(id_usuari,blob); 
  
  
  return true; 
}



